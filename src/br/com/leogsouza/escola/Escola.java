package br.com.leogsouza.escola;

import br.com.leogsouza.escola.aluno.Aluno;

public class Escola {

	Aluno[] alunos = new Aluno[10];
	private int totalAlunos = 1;
	private String nome;
	
	/**
	 * M�todo que adiciona um aluno na escola
	 * Se a quantidade de alunos for maior que o tamanho inicial
	 * � criado um novo array com mais 10 posi��es
	 * @param a
	 */
	public void adiciona(Aluno a) {
		if(this.getTotalAlunos() <= this.alunos.length) {
			this.alunos[this.getTotalAlunos() -1] = a;
			this.setTotalAlunos(this.getTotalAlunos() + 1);
		} else {
			Aluno[] tempAlunos = new Aluno[this.alunos.length + 10];
			for(int i =0; i < this.alunos.length; i++) {
				tempAlunos[i] = this.alunos[i];
			}
			tempAlunos[this.getTotalAlunos() - 1] = a;
			
			this.alunos = tempAlunos;
			this.setTotalAlunos(this.getTotalAlunos() + 1);
		}
	}

	/**
	 * @return the totalAlunos
	 */
	public int getTotalAlunos() {
		return totalAlunos;
	}

	/**
	 * @param totalAlunos the totalAlunos to set
	 */
	public void setTotalAlunos(int totalAlunos) {
		this.totalAlunos = totalAlunos;
	}
	
	public double getMedia() {
		
		return 0.0;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void mostrarMedia() {
		double totalNotas = 0;
		
		for(int cont = 0; cont < this.alunos.length;cont++) {
			if(this.alunos[cont] != null) {
				totalNotas += this.alunos[cont].getNota();
			}			 
		}
		
		double mediaNotas = totalNotas / this.getTotalAlunos();
		
		System.out.println("A m�dia das notas � :"+ mediaNotas);
		
	}
}
