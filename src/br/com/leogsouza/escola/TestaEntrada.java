package br.com.leogsouza.escola;

import java.util.Scanner;

import br.com.leogsouza.escola.aluno.Aluno;

public class TestaEntrada {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Escola escola = new Escola();
		Aluno[] alunos = new Aluno[10];
		System.out.println("Digite o nome da Escola: ");
		escola.setNome(scanner.nextLine());

		int opcao = 1;
		while (opcao != 0) {
			System.out.println("Escolha uma das op��es:");
			System.out.println("1 - Incluir aluno");
			System.out.println("2 - Mostrar m�dia");
			System.out.println("0 - Sair");
			
			opcao = scanner.nextInt();
			
			if(opcao == 1) {
				Aluno aluno = new Aluno();
				System.out.println("Informe o nome do aluno: ");
				
				String nomeAluno = scanner.next();
				aluno.setNome(nomeAluno);
				System.out.println("Informe a nota do aluno: ");
				
				double notaAluno = scanner.nextDouble(); 
				aluno.setNota(notaAluno);
				
				escola.adiciona(aluno);
				System.out.println("Aluno inserido com sucesso");
			}
			
			else if(opcao == 2){
				escola.mostrarMedia();
			}
		}

		System.out.println("O nome da escola � : ");

	}

}
